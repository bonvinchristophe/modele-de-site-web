---
author: Christophe Bonvin
title: Actualités
---

# L'actualité de l'orientation
A partir de novembre- décembre, les établissements vont communiquer sur les dates de portes ouvertes, des journées d'immersion...
Les informations que nous recevons durant cette période seront centralisées ici, elles seront classées selon les formations et les établissements concernés.

!!! Info "Rappel - Les dates importantes"
    La journée portes ouvertes des universités aura lieu le **samedi 25 janvier 2025**, quant à celle des lycées (CPGE, BTS) aura lieu le **samedi 1er février 2025 pour les lycées hors métropole de Lille** et le **samedi 25 janvier 2025 pour les lycées de la métropole lilloise**.

!!! Info "Les salons de l'étudiant dans la région"
    Vous trouverez les dates et lieux de différents salons de l'étudiant de la région en [allan sur ce site](https://www.letudiant.fr/etudes/salons/ville-arras+dunkerque+lille+valenciennes.html){:target="_blank" }

## :information_source: Les Universités (BUT / licence)
### :arrow_right: Université du Littoral et de la Côte d'Opale (ULCO)

L'université propose des visioconférences, qui sont des temps d'échange avec les responsables de formation et des personnels d'orientation, sur différentes thématiques :
??? Info "Mercredi 4 décembre de 14h à 16h : Les journées d’immersion"
    Pour vous connecter, vous pouvez le faire via le lien suivant : [https://univ-littoral-fr.zoom.us/j/99191538936?pwd=YPnZCcUoDmtZ4wbwWsTrtMzTVUjUyY.1](https://univ-littoral-fr.zoom.us/j/99191538936?pwd=YPnZCcUoDmtZ4wbwWsTrtMzTVUjUyY.1){:target="_blank" }

    ID de réunion : 991 9153 8936

    Code secret : 403822

??? Info "Mercredi 11 décembre de 14h à 16h : Les études en BUT tertiaires"
    Pour vous connecter, vous pouvez le faire via le lien suivant : [https://univ-littoral-fr.zoom.us/j/99191538936?pwd=YPnZCcUoDmtZ4wbwWsTrtMzTVUjUyY.1](https://univ-littoral-fr.zoom.us/j/99191538936?pwd=YPnZCcUoDmtZ4wbwWsTrtMzTVUjUyY.1){:target="_blank" }

    ID de réunion : 991 9153 8936

    Code secret : 403822

??? Info "Mercredi 18 décembre de 14h à 16h : Les études en Langues"
    Pour vous connecter, vous pouvez le faire via le lien suivant : [https://univ-littoral-fr.zoom.us/j/99191538936?pwd=YPnZCcUoDmtZ4wbwWsTrtMzTVUjUyY.1](https://univ-littoral-fr.zoom.us/j/99191538936?pwd=YPnZCcUoDmtZ4wbwWsTrtMzTVUjUyY.1){:target="_blank" }

    ID de réunion : 991 9153 8936

    Code secret : 403822   

??? Info "Mercredi 15 janvier de 14h à 16h : Présentation de Parcoursup"
    Pour vous connecter, vous pouvez le faire via le lien suivant : [https://univ-littoral-fr.zoom.us/j/99191538936?pwd=YPnZCcUoDmtZ4wbwWsTrtMzTVUjUyY.1](https://univ-littoral-fr.zoom.us/j/99191538936?pwd=YPnZCcUoDmtZ4wbwWsTrtMzTVUjUyY.1){:target="_blank" }

    ID de réunion : 991 9153 8936

    Code secret : 403822

??? Info "Mercredi 22 janvier de 14h à 16h : Les études et formations en alternance"
    Pour vous connecter, vous pouvez le faire via le lien suivant : [https://univ-littoral-fr.zoom.us/j/99191538936?pwd=YPnZCcUoDmtZ4wbwWsTrtMzTVUjUyY.1](https://univ-littoral-fr.zoom.us/j/99191538936?pwd=YPnZCcUoDmtZ4wbwWsTrtMzTVUjUyY.1){:target="_blank" }

    ID de réunion : 991 9153 8936

    Code secret : 403822

Cette année, le Service d’orientation de l’université propose par ailleurs deux lives prioritairement destinés aux parents afin de présenter les dispositifs d’accompagnement et d’aide à la réussite mis en place par l’ULCO :

??? Info "Jeudi 28 novembre de 18h à 19h30"
    Pour vous connecter, vous pouvez le faire via le lien suivant : [https://univ-littoral-fr.zoom.us/j/92030879208?pwd=J5dMawMaZxg8LJ3XXmu4q4WiV2R5iW.1](https://univ-littoral-fr.zoom.us/j/92030879208?pwd=J5dMawMaZxg8LJ3XXmu4q4WiV2R5iW.1){:target="_blank" }

    ID de réunion : 920 3087 9208

    Code secret : 792277

??? Info "Jeudi 9 janvier de 18h à 19h30"
    Pour vous connecter, vous pouvez le faire via le lien suivant : [https://univ-littoral-fr.zoom.us/j/92030879208?pwd=J5dMawMaZxg8LJ3XXmu4q4WiV2R5iW.1](https://univ-littoral-fr.zoom.us/j/92030879208?pwd=J5dMawMaZxg8LJ3XXmu4q4WiV2R5iW.1){:target="_blank" }

    ID de réunion : 920 3087 9208

    Code secret : 792277

### :arrow_right: Université de Lille
* **Rencontres Parents et lycéens**

Ces rencontres permettent aux lycéens et à Ieurs parents de mieux appréhender l’arrivée dans l’enseignement supérieur, de découvrir les différents aspects de la vie universitaire, et de faire un focus sur un champ disciplinaire enseigné à l’université.
Elles peuvent avoir lieu sur le campus et sont retransmises en replay vidéo

??? Info "Mardi 26 novembre 2024 à 18h - LILLIAD Learning Center, Campus Cité Scientifique"
    Plus d'informations sur les détails de ces rencontres en allant sur ce site : [https://avouslesup.univ-lille.fr/](https://avouslesup.univ-lille.fr/){:target="_blank" }

??? Info "Mercredi 4 décembre 2024 à 18h — Faculté de Médecine Henri Warembourg"
    Plus d'informations sur les détails de ces rencontres en allant sur ce site : [https://avouslesup.univ-lille.fr/](https://avouslesup.univ-lille.fr/){:target="_blank" }

* **Les immersions:**

Ces journées réservées aux élèves de Terminale permettent de découvrir une formation et son campus, et d’échanger avec des étudiants et enseignants.

??? Info "Première session : du 19 au 21 novembre 2024"
    Plus d'informations sur les détails de ces journées en allant sur ce site : [https://immersion.univ-lille.fr/](https://immersion.univ-lille.fr/){:target="_blank" }

??? Info "Seconde session : du 11 au 13 février 2025"
    Plus d'informations sur les détails de ces journées en allant sur ce site : [https://immersion.univ-lille.fr/](https://immersion.univ-lille.fr/){:target="_blank" }

### :arrow_right: Université d'Artois
L'université d'Artois propose également des immersions en allant sur ce site : [https://immersion.univ-artois.fr/search](https://immersion.univ-artois.fr/search){:target="_blank" }

??? Info "Plus d'informations"
    Plus d'informations sur les détails de ces journées en allant sur ce site : [https://www.univ-artois.fr/](https://www.univ-artois.fr/){:target="_blank" }

### :arrow_right: Université Catholique de Lille
Vous trouverez des informations sur les immersions en allant sur ce site : [https://www.univ-catholille.fr/construire-son-projet/immersions/](https://www.univ-catholille.fr/construire-son-projet/immersions/){:target="_blank" }

## :information_source: Les CPGE (classes préparatoires aux grandes écoles)
Voici la liste de la plupart des [CPGE de la région](https://fr.wikipedia.org/wiki/Liste_des_classes_pr%C3%A9paratoires_aux_grandes_%C3%A9coles#Nord-Pas-de-Calais){:target="_blank" }

!!! Danger "Mise à jour"
    Les dates des immersions arrivent parfois tardivement. Il ne faut pas hésiter à consulter les sites web de ces classes préparatoires pour trouver les coordonées du lycée et à prendre contact avec eux pour demander une immersion.

??? Tip "CPGE Robespierre Arras"
    Plus d'informations sur les détails de ces journées en allant sur ce site : [http://www.cpge.lycee-robespierre.fr/index.php?view=article&id=55:forum-des-ecoles-et-jpo-2022&catid=8:page-d-accueil](http://www.cpge.lycee-robespierre.fr/index.php?view=article&id=55:forum-des-ecoles-et-jpo-2022&catid=8:page-d-accueil){:target="_blank" }

??? Tip "CPGE Gustave Eiffel Armentières"
    Plus d'informations sur les détails de ces journées en allant sur ce site : [https://cpge.lycee-gustave-eiffel.fr/immersion-cpge/](https://cpge.lycee-gustave-eiffel.fr/immersion-cpge/){:target="_blank" }

??? Tip "CPGE Faidherbe Lille"
    Plus d'informations sur les détails de ces journées en contactant directement l'établissement. Pour cela rendez vous sur le site : [https://cpge.lycee-gustave-eiffel.fr/immersion-cpge/](https://cpge.lycee-gustave-eiffel.fr/immersion-cpge/){:target="_blank" } pour trouver les coordonnées de contact.

??? Tip "CPGE Chatelet Douai"
    Plus d'informations sur les détails de ces journées en contactant directement l'établissement. Pour cela rendez vous sur le site : [https://chatelet-douai.fr/index.php/category/classes-preparatoires/](https://chatelet-douai.fr/index.php/category/classes-preparatoires/){:target="_blank" } pour trouver les coordonnées de contact.

??? Tip "CPGE Jean Bart Dunkerque"
    Plus d'informations sur les détails de ces journées en contactant directement l'établissement. Pour cela rendez vous sur le site : [https://prepa.lyceejeanbart.fr/](https://prepa.lyceejeanbart.fr/){:target="_blank" } pour trouver les coordonnées de contact.

??? Tip "CPGE Gambetta Carnot Arras"
    Plus d'informations sur les détails de ces journées en contactant directement l'établissement. Pour cela rendez vous sur le site : [https://gambetta-carnot.prepa-arras.fr/](https://gambetta-carnot.prepa-arras.fr/){:target="_blank" } pour trouver les coordonnées de contact.

??? Tip "CPGE Wallon Valenciennes"
    Plus d'informations sur les détails de ces journées en contactant directement l'établissement. Pour cela rendez vous sur le site : [https://lyceehenriwallon-valenciennes.fr/classes-prepas](https://lyceehenriwallon-valenciennes.fr/classes-prepas){:target="_blank" } pour trouver les coordonnées de contact.




## :information_source: Les BTS
Etant donné la diversité des BTS qui existent, la liste des informations sur les immersions concernant les BTS sera limitée. Il est vivement conseillé de se renseigner directement auprès des lycées si vous êtes intéressés par une journée d'immersion.

??? Tip "Lycée Béhal Lens"
    Pour participer à une journée d'immersion, il faut remplir le formulaire présent à [cette adresse](https://docs.google.com/forms/d/e/1FAIpQLSeRkwCo0GpuPRVM5S7_UyNB0Wj8B4FXihkhkoX3EMnwlqJz7w/viewform){:target="_blank" }


## :information_source: Autres formations (DE, IFSI, etc.)
Informations à venir