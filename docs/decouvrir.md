---
author: Christophe Bonvin
title: Les temps forts de l'orientation
---

# Les temps forts de l'orientation
Les établissements d'enseignement supérieur ouvrent leurs portes tous les ans au cours de leurs portes ouvertes. De plus, elles sont de plus en plus nombreuses à proposer des journées d'immersion qui permettent aux élèves de suivre une journée de cours mais également de discuter avec des étudiants. 
En plus de cela, il exsite des évènements tout au long de l'année que sont les forums de l'orientation ou salons de l'étudiant.

!!! warning "Qui est concerné ?"
    Les journées d'immersion sont réservées en priorité aux élèves de terminales (elles sont rarement proposées aux élèves de première). En revanche, les journées portes ouvertes et les forum de l'orientation sont ouverts à tous. Il est **vivement** conseillé d'y participer avant la terminale afin de se familiariser avec le post-bac et commencer à se projetter. C'est aussi l'occasion de discuter avec des étudiants et des enseignants et découvrir des formations qui pourraient nous intéresser.


## :information_source: Les salons de l'étudiant
Rendez-vous incontournables de l'orientation, ces salons regroupent dans un même lieu différents établissement de formations. Vous pourrez discuter avec des enseignants et de jeunes étudiants et leur poser toutes les questions que vous vous posez.

!!! Note "Accès à la ressource"
    Vous trouverez une liste à jour des salons dans la région [sur ce site](https://www.letudiant.fr/etudes/salons/ville-arras+dunkerque+lille+valenciennes.html){:target="_blank" }

??? Danger "Etablissements privés - Etablissements publics"

    Dans les salons et forum de l'orientation, on note une part de plus en plus importante d'établissements ou écoles privés. Certaines proposent des inscriptions en dehors de parcoursup ce qui peut être rassurant pour les familles. Toutefois, il convient d'être vigilant sur le contenu des cours, les débouchés, de vérifier si le diplome et l'école sont reconnus par le ministère de l'enseignement supérieur. 

    Il existe très souvent des alternatives dans des établissements publics qui ont l'avantage d'avoir des frais de scolarité peu élevés (voire pas de frais pour les boursiers dans la plupart des cas), et d'être obligatoirement reconnus par l'état. Ils délivrent donc des diplomes reconnus sur tout le territoire et même en europe pour la plupart des diplomes universitaires.

    **N'hésitez pas à en parler aux professeurs principaux de vos enfants**

    *Pour être reconnu par l'état, le diplome proposé doit être enregistré au RNCP. [Cette page Web](https://www.francecompetences.fr/fiche/votre-futur-diplome-est-il-reconnu-par-letat/) vous explique comment le vérifier.*


## :information_source: Les journées portes ouvertes (JPO)
Véritables temps forts des établissements, les journées portes ouvertes permettent aux élèves et à leurs familles de découvrir les établissements d'enseignement supérieur en visitant les locaux et en rencontrant le personnel éducatif. Ces événements offrent l'occasion d'assister à des présentations sur les filières proposées, d'échanger avec des étudiants actuels et de poser des questions sur la vie étudiante. Elles visent à informer les futurs étudiants sur les choix académiques disponibles et à les aider à se projeter dans leur parcours.


!!! Info "Les dates importantes"
    La journée portes ouvertes des universités aura lieu le **samedi 25 janvier 2025**, quant à celle des lycées (CPGE, BTS) aura lieu le **samedi 1er février 2025 pour les lycées hors métropole de Lille** et le **samedi 25 janvier 2025 pour les lycées de la métropole lilloise**.


## :information_source: Les journées d'immersion
Les journées d'immersion permettent aux élèves de découvrir l'enseignement supérieur en visitant divers établissements et en rencontrant des étudiants. Elles offrent l'opportunité d'assister à des cours et d'explorer différentes filières, aidant ainsi les élèves à faire des choix éclairés pour leur avenir académique. Ces journées visent à stimuler la motivation, à créer des liens entre le secondaire et le supérieur, et à sensibiliser les élèves aux exigences de l'enseignement supérieur. En somme, elles constituent une étape clé pour préparer les élèves à leur future orientation.

!!! Danger "Les différentes dates des salons, portes ouvertes et immersions dans la région"
    L'ensemble des ces informations est disponible et régulièrement mis à jour sur [la page Actualités de ce site](https://bonvinchristophe.forge.apps.education.fr/orientation-VDL/actualites/){:target="_blank" }


