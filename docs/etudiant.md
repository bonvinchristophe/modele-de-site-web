---
author: Christophe Bonvin
title: Démarches lées à la vie étudiante
---

# Les démarches liées à la vie étudiante

## :information_source: Les bourses
Le Dossier Social Étudiant (DSE) est un outil essentiel pour les étudiants souhaitant bénéficier d'une aide financière pour leurs études supérieures. Il permet de demander des bourses sur critères sociaux et des aides au logement, en tenant compte de la situation financière de la famille. La constitution du DSE se fait en ligne et nécessite la fourniture de documents justificatifs. Un DSE complet et bien préparé augmente les chances d'obtenir un soutien financier, facilitant ainsi l'accès à l'enseignement supérieur.

!!! Note "Constituer le dossier social étudiant"
    Pour réaliser une demande de bourse, il convient de faire un Dossier Social Etudiant. Toutes les informations nécessaires sur la démarche sont [disponibles à cette adresse](https://www.service-public.fr/particuliers/vosdroits/F12216){:target="_blank" }

!!! tip "Simulateur de bourses"
    Vous pouvez simuler une demande de bourse pour savoir si vous pouvez y prétendre. Vous obtiendrez également un montant de la bourse à titre indicatif.
    L'accès au simulateur se fait [en cliquant ici](https://www.lescrous.fr/nos-services/une-offre-de-services-riche-et-de-qualite-pour-tous-les-etudiants/#simulateur-de-bourse-testez-votre-eligibilite){:target="_blank" }

## :information_source: Les logements étudiants
Les logements étudiants sont des solutions d'hébergement spécialement conçues pour répondre aux besoins des étudiants durant leur parcours académique. Ils incluent des résidences universitaires, des studios, et des colocations, offrant un cadre adapté à la vie étudiante. Ces logements sont souvent situés à proximité des établissements d'enseignement, facilitant ainsi les déplacements.


!!! Note "Trouver un logement"
    Il est possible de trouver une location chez un particulier mais le Crous propose des logements ainsi qu'une plateforme de recherche d'hébergements. Elle est accessible [à cette adresse](https://messervices.etudiant.gouv.fr/){:target="_blank" }
   

??? Info " Qu'est ce que le CROUS?"
    Le CROUS (Centre Régional des Œuvres Universitaires et Scolaires) est un organisme public en France qui joue un rôle clé dans la vie étudiante. Il est chargé de gérer les bourses sur critères sociaux, les logements étudiants, et les services de restauration universitaire. Le CROUS offre également des activités culturelles et sportives, ainsi que des conseils pour l'orientation et l'insertion professionnelle. En facilitant l'accès à des ressources essentielles, le CROUS contribue à améliorer les conditions de vie des étudiants et à favoriser leur réussite académique. En somme, le CROUS est un acteur central du soutien aux étudiants dans leur parcours éducatif.

## :information_source: Aide à la mobilité 
C’est une aide financière de 500 € pour les futurs étudiants qui bénéficient d'une bourse de lycée et qui souhaitent s'inscrire, via Parcoursup, dans une formation située hors de leur académie de résidence.

!!! Note "Plus d'informations"
    Vous trouverez les conditions d'attribution et les démarches [à cette adresse](https://amp.etudiant.gouv.fr/){:target="_blank" }
    

## :information_source: La CVEC (Contribution de Vie Etudiante et de Campus)
La CVEC est la Contribution de vie étudiante et de campus. La loi prévoit qu'elle est collectée par les Crous. D'un montant de 103 € en 2024/25, on peut y être assujetti ou en être exonéré en fonction des cas.

!!! Note "Plus d'informations"
    Vous trouverez les démarches et les cas d'exonération [à cette adresse](https://www.etudiant.gouv.fr/fr/cvec-une-demarche-de-rentree-incontournable-955){:target="_blank" }


!!! warning "Site de référence"
    Il y a d'autres aspects à découvrir comme la restauration, la mobilité, la santé, etc. Vous pouvez trouver toutes les informations utiles [à cette adresse](https://messervices.etudiant.gouv.fr/)