---
author: Christophe Bonvin
title: Les formations Post-Bac
---

# Les formations post-bac

## Introduction 

Il existe de nombreuses de voies de formation en post-bac avec chacune leurs spécificités, il peut donc être difficile de s'y retrouver. Cette page recense un ensemble de sites qui vous permettront de vous familiariser avec les formations.

## :information_source: Connaître les différentes formations post-bac
L'ONISEP est un site de référence en la matière. Il regroupe l'ensemble des formations qui existent en expliquant chacune leurs spécificités (études courtes ou longues, formation généraliste ou professionnalisante, en lycée ou en université, etc.)

!!! Note "Accès à la ressource"
    Vous trouverez ces informations en allant [sur ce site](https://www.onisep.fr/formation/apres-le-bac-les-etudes-superieures/les-principales-filieres-d-etudes-superieures){:target="_blank" }

## :information_source: Rechercher des métiers
L'ONISEP a créé un site qui répertorie un nombre important de professions ou de domaines. Vous pouvez filtrer les résultats de la recherche en fonction de vos centres d'intérêt, de la durée des études. Vous pourrez ensuite trouver les établissements qui proposent la formation adéquate. 

!!! Note "Accès à la ressource"
    Vous trouverez ces informations en allant [sur ce site](https://www.onisep.fr/recherche?context=metier){:target="_blank" }

??? note "Le schéma des études supérieures"
    [![](https://minio.apps.education.fr/codimd-prod/uploads/upload_d6421c47589b8ff4f1e9895a450dabc0.png)](https://www.onisep.fr/media/files/pdf/schemas-des-etudes-pour-rubrique-telechargement-des-guides/le-schema-des-etudes-apres-le-bac-nov.-2024){:target="_blank" }

## :information_source: Le CDI
Le CDI du lycée possède un nombre conséquent de documents en relation avec l'orientation, notamment des revues thématiques sur les professions. Ils sont consultables au CDI

!!! Note "Accès à la ressource"
    Vous pouvez consulter les titres en allant [sur cette page](https://0597005u.esidoc.fr/recherche/onisep){:target="_blank" }
 

## :information_source: Le cas de la réorientation
Des étudiants de 1ʳᵉ année peuvent être amenés à changer de filière ou à quitter l’université pour un BTS, une école… S’il est possible de se réorienter dès la fin du 1ʳᵉ semestre, il y a des modalités à respecter pour postuler et être admis en formation. 

!!! Note "Accès à la ressource"
    Vous pouvez consulter un article sur ce thème en allant [sur cette page](https://www.onisep.fr/formation/apres-le-bac-les-etudes-superieures/savoir-rebondir-se-reorienter){:target="_blank" }
 

??? tip "Important"
    Une réorientation n'est pas forcément synonyme d'échec, il peut arriver que le projet d'orientation change ou alors que la formation obtenue ne corresponde pas aux attentes. Il est alors important de se tourner vers les CIO ou les SCUIO des universités pour obtenir des informations et des conseils.

    De plus, conserver le travail effectué en AP, il pourrait être très utile en cas de réorientation en faisant gagner du temps sur la recherche d'informations.


