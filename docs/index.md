---
author: Christophe Bonvin
title: 🏡 Accueil
---

# Orientation post-bac - Lycée Polyvalent du Val de Lys

Bienvenue sur le site du lycée du Val de Lys destiné à l'orientation du lycée polyvalent du Val de Lys. Il est destiné aux parents et aux élèves de classe de terminale. Il peut être aussi très utile pour ceux des classes de seconde et de première.

Voic les principaux thèmes qui seront abordés : 

- [Parcoursup et son fonctionnement](https://bonvinchristophe.forge.apps.education.fr/orientation-VDL/parcoursup/)
- [les études en post-bac](https://bonvinchristophe.forge.apps.education.fr/orientation-VDL/formations/)
- [les portes ouvertes, salons ou immersions](https://bonvinchristophe.forge.apps.education.fr/orientation-VDL/decouvrir/)
- [des informations sur la vie étudiante (bourses, logement, etc.)](https://bonvinchristophe.forge.apps.education.fr/orientation-VDL/etudiant/)

Il y a également la présence d'un onglet [*Actualités*](https://bonvinchristophe.forge.apps.education.fr/orientation-VDL/actualites/) qui regroupent les dernières informations sur l'orientation, notamment des dates de portes ouvertes, de salons ou d'immersion.

!!! Danger "Information importante"

    Ce site été conçu pour vous apporter une aide pour l'orientation en post-bac, en centralisant les informations les plus pertinentes. Il ne remplace pas le travail des enseignants (notamment en AP avec les professeurs principaux), ni des Psy-EN qui sont vos interlocuteurs privilégiés.

!!! Note "Rappel"

    Pour prendre rendez vous avec les PSY-EN (conseillères d'orientation), veuillez vous adresser à la vie scolaire.