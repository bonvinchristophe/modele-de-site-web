---
author: Christophe Bonvin
title: La plateforme Parcoursup
---

# La plateforme Parcoursup

## Présentation

La plateforme Parcoursup permet de candidater parmi plus de 23 000 formations de l’enseignement supérieur.

Dans le cadre d’un calendrier unique, vous pouvez à travers un seul dossier :

- consulter tout l’offre de formation post bac ;
- trouver les informations essentielles pour votre projet d’études supérieures : le statut des formations, les frais de scolarité, les critères d’analyse des candidatures; le niveau de la demande ou encore les taux de réussite/d’insertion professionnelle ;
- constituer votre dossier, qui sera ensuite transmis aux formations que vous avez sélectionnées, en faisant valoir votre motivation, vos centres d’intérêts etc ;
- formuler librement des vœux pour les formations qui vous intéressent ;
- recevoir, après l’examen de votre candidature par les formations, des propositions d’admission et choisir l’établissement où vous ferez votre rentrée.

## :information_source: Les échéances
Voici les dates à respecter pour l'année 2024-2025

Il y a 3 périodes importantes : 

- La découverte des formations : **de novembre 2024 à janvier 2025**
- L'inscription à la plateforme pour formuler des voeux : **du 15 janvier au 13 mars**,  puis la finalisation du dossier **juqu'au 2 avril 2025**
- La consultation et la validation des propositions : **du 2 juin au 10 juillet 2025** 
  
!!! Note "Accès à la ressource"
    Vous trouverez davantage d'informations sur les échéances en allant [sur ce site](https://www.parcoursup.gouv.fr/calendrier#fr-s-accordion-100-1-32){:target="_blank" }

!!! Danger "Respect des délais"

    Les dates sont à **respecter scrupuleusement**. Il est donc **déconseillé** de réaliser les démarches la veille de la date butoir. En effet, il peut y avoir des problèmes de serveur suite à une forte affluence et cela pourrait compromettre l'inscription ou la saisie des voeux.

??? tip ":new: Mémo Parcoursup - Mise à jour du 26/11/2025 :new:"
    **Cliquer sur l'image pour télécharger le fichier pdf**
    [![](https://minio.apps.education.fr/codimd-prod/uploads/upload_f1ce8f1dc42ec22529f61bc84c001c63.png)](download/memo_eleves_2025.pdf){:target="_blank" }
    
??? tip ":new: Livret d'accompagnement - Mise à jour du 26/11/2025 :new:"
    **Cliquer sur l'image pour télécharger le fichier pdf**
    [![](https://minio.apps.education.fr/codimd-prod/uploads/upload_54600ddcb5c79d23321ea06b331e1914.png)](download/livret_2025.pdf){:target="_blank" }
    



## :information_source: Rechercher des formations
La plateforme parcoursup répertorie les différentes formations du supérieur dans toute la France. C'est un outil complet qui permet de connaître les attentes des formations, leur localisation, le nombre de places disponibles, le taux d'accès, des statistiques sur le rythme d'envoi des propositions d'admission, etc.

!!! Note "Accès à la ressource"
    - Vous trouverez la carte des formations à [cette adresse](https://dossier.parcoursup.fr/Candidat/carte){:target="_blank" }
    
    - Un exemple d'un fiche pour la licence de psychologie à Lille est [disponible ici](https://dossier.parcoursup.fr/Candidats/public/fiches/afficherFicheFormation?g_ta_cod=7095&typeBac=0&originePc=0){:target="_blank" }


## :information_source: Des ressources pour prendre en main Parcoursup

1. Une Foire Aux Questions (FAQ) a été mise en place sur le site de Parcoursup, elle regroupe différentes thématiques et plus particulièrement sur la phase d'admission.
2. Un formulaire de contact est aussi disponible sur le site
3. L'ONISEP a mis un ligne de nombreuses vidéos de présentation de la plateforme et d'autres sur la formulation des voeux dans différentes filières.

!!! Note "Accès aux ressources"
    1. La FAQ de parcoursup est [disponible ici](https://www.parcoursup.gouv.fr/outils-et-ressources)
    2. Le formulaire de contact est [accessible à cette adresse](https://www.parcoursup.gouv.fr/contact)
    3. Les vidéos de l'ONISEP sont [visionnables ici](https://oniseptv.onisep.fr/recherche?text=parcoursup&context=tv)

!!! info "Utilisation avec les élèves"
    Les professeurs principaux epliqueront aux élèves le fonctionnement de la plateforme. De plus, une réunion d'information à destination des parents aura lieu au lycée (un mail sera envoyé aux parents pour leur indiquer la date et l'heure).
